// MegaBase 2018
#ifndef MEGABASE_HANDLER_TOP
#define MEGABASE_HANDLER_TOP
#include <memory>

namespace cpp_event
{
    namespace http
    {
        class Request;
    } // namespace http
} // namespace cpp_event

namespace mega_base
{
    class TopKeysAnalyser;

    namespace handler
    {
        class Top
        {
        public:
            explicit Top(TopKeysAnalyser &top) noexcept;

            void operator()(std::unique_ptr<cpp_event::http::Request> req) noexcept;

        private:
            TopKeysAnalyser &top_;
        };
    } //namespace handler
} // namespace mega_base
#endif // MEGABASE_HANDLER_TOP
