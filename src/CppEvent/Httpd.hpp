// MegaBase 2018
#ifndef CPPEVENT_HTTPD
#define CPPEVENT_HTTPD
#include "CppEvent/Http/Request.hpp"

#include <event2/http.h>

#include <cassert>
#include <memory>

namespace cpp_event
{
    class Base;

    class Httpd
    {
    public:
        explicit Httpd(Base &base) noexcept;

        void accept(std::uint16_t port);

        template <typename Handler>
        void attach(Handler &handler, const char *path) noexcept
        {
            const int res{evhttp_set_cb(httpd_.get(), path,
                [](evhttp_request *req, void *arg) {
                    (*static_cast<Handler *>(arg))(http::Request::build(req));
                },
                &handler)};
            (void)res;
            assert(res == 0);
        }

        using EvHttp = std::unique_ptr<evhttp, void (*)(evhttp *)>;
        const EvHttp &getEvHttp() const noexcept;

    private:
        EvHttp httpd_;
    };
} // namespace cpp_event
#endif //CPPEVENT_HTTPD
