// MegaBase 2018
#ifndef MEGABASE_HANDLER_GET
#define MEGABASE_HANDLER_GET
#include "CppEvent/Http/Request.hpp"
#include "MegaBase/Handler/BaseDBHandler.hpp"

#include <iostream>
#include <memory>

namespace mega_base
{
    namespace handler
    {
        template <typename... Observers>
        class Get : private BaseDBHandler<Observers...>
        {
        private:
            using BaseClass = BaseDBHandler<Observers...>;

        public:
            using BaseClass::BaseDBHandler;

            void operator()(std::unique_ptr<cpp_event::http::Request> req) noexcept
            {
                try
                {
                    const char *key{req->getIncomingParam("key")};
                    if (!key)
                    {
                        cpp_event::http::Request::sendReply(std::move(req), cpp_event::http::Codes::BAD_REQUEST);
                        return;
                    }
                    const auto &value = BaseClass::engine_.get(key);
                    if (!value.hasData())
                    {
                        cpp_event::http::Request::sendReply(std::move(req), cpp_event::http::Codes::NO_CONTENT);
                        return;
                    }
                    cpp_event::http::Request::sendReply(std::move(req),
                        cpp_event::http::Codes::OK, {value.data(), value.size()});
                }
                catch (const std::exception &ex)
                {
                    std::cerr << "Got an exception on /get request; " << ex.what() << std::endl;
                    if (req)
                    {
                        cpp_event::http::Request::sendReply(std::move(req), cpp_event::http::Codes::SERVER_ERROR);
                    }
                }
            }
        };
    } // namespace handler
} // namespace mega_base

#endif //MEGABASE_HANDLER_GET
