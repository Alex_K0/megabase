// MegaBase 2018
#include "MegaBase/TopKeysAnalyser.hpp"

#include "MegaBase/Engine.hpp"
#include "MegaBase/StringView.hpp"

#include <array>
#include <cassert>
#include <set>

namespace mega_base
{
    constexpr std::uint32_t TopKeysAnalyser::Stat::KEEP_SECONDS;

    TopKeysAnalyser::Stat::Stat() noexcept
    {
        histogram_.fill(0u);
    }

    void TopKeysAnalyser::Stat::inc(std::chrono::seconds currentTime) noexcept
    {
        if (currentTime <= lastAccess_)
        {
            if (lastAccess_ - currentTime < std::chrono::seconds{KEEP_SECONDS})
            {
                ++totalValue_;
                ++histogram_[currentTime.count() % KEEP_SECONDS];
            }
            return;
        }
        dropUnused(currentTime);
        lastAccess_ = currentTime;
        std::uint32_t &secValue{histogram_[lastAccess_.count() % KEEP_SECONDS]};
        totalValue_ -= secValue;
        secValue = 1u;
        ++totalValue_;
    }

    void TopKeysAnalyser::Stat::dropUnused(std::chrono::seconds currentTime) noexcept
    {
        if (lastAccess_ >= currentTime)
        {
            return;
        }

        if (outdated(currentTime))
        {
            totalValue_ = 0u;
            histogram_.fill(0u);
            return;
        }

        for (auto unused = lastAccess_ + std::chrono::seconds{1}; unused != currentTime;
             unused += std::chrono::seconds{1})
        {
            std::uint32_t &secValue{histogram_[unused.count() % KEEP_SECONDS]};
            totalValue_ -= secValue;
            secValue = 0u;
        }
    }

    bool TopKeysAnalyser::Stat::outdated(std::chrono::seconds currentTime) const noexcept
    {
        return currentTime >= lastAccess_ + std::chrono::seconds{KEEP_SECONDS};
    }

    TopKeysAnalyser::LowValueStats::LowValueStats() noexcept
    {
        cache.fill(nullptr);
    }

    void TopKeysAnalyser::LowValueStats::erase(Stat *stat) noexcept
    {
        const std::size_t index{reinterpret_cast<std::uintptr_t>(stat) % cache.size()};
        if (cache[index] == stat)
        {
            cache[index] = nullptr;
        }
    }

    void TopKeysAnalyser::LowValueStats::tryEmplace(Stat *stat) noexcept
    {
        const std::size_t index{reinterpret_cast<std::uintptr_t>(stat) % cache.size()};
        if (cache[index] == nullptr)
        {
            cache[index] = stat;
        }
    }

    TopKeysAnalyser::TopKeysAnalyser(std::size_t memoryLimit)
        : memoryLimit_(memoryLimit),
          lruList_(LruList::allocator_type{structsMemoryUsage_}),
          stats_(StatsStorage::allocator_type{structsMemoryUsage_})
    {
    }

    void TopKeysAnalyser::updateCurrentTime() noexcept
    {
        const auto sinceEpoch = std::chrono::steady_clock::now().time_since_epoch();
        currentTime_ = std::chrono::duration_cast<std::chrono::seconds>(sinceEpoch);
    }

    TopKeysAnalyser::Stat &TopKeysAnalyser::insertStat(const StringView &key)
    {
        Stat &stat{stats_[key]};
        stat.lruIt = lruList_.emplace(lruList_.begin(), key);
        return stat;
    }

    TopKeysAnalyser::Stat *TopKeysAnalyser::getStat(const StringView &key)
    {
        auto it = stats_.find(key);
        if (it != stats_.end())
        {
            if (key.storedInPool() && it->second.keyNotFromPool)
            {
                lowValueStats_.erase(&it->second);
                Stat movedStat{std::move(it->second)};
                stats_.erase(it);
                *movedStat.lruIt = key;
                movedStat.keyNotFromPool.reset();
                keysMemoryUsage_ -= key.size();
                it = stats_.emplace(key, std::move(movedStat)).first;
            }
            lruList_.splice(lruList_.begin(), lruList_, it->second.lruIt);
            return &it->second;
        }

        if (key.storedInPool())
        {
            return fitMemory(getStatApproximateMemoryUsage()) ? &insertStat(key) : nullptr;
        }

        if (!fitMemory(getStatApproximateMemoryUsage() + key.size()))
        {
            return nullptr;
        }
        auto keyNotFromPool = std::make_unique<char[]>(key.size());
        keysMemoryUsage_ += key.size();
        std::memcpy(keyNotFromPool.get(), key.data(), key.size());
        Stat &stat{insertStat(StringView{keyNotFromPool.get(), key.size()})};
        stat.keyNotFromPool = std::move(keyNotFromPool);
        return &stat;
    }

    std::size_t TopKeysAnalyser::getStatApproximateMemoryUsage() const noexcept
    {
        return stats_.empty()
                   ? sizeof(StatsStorage::value_type) + sizeof(LruList::value_type)
                   : 1 + structsMemoryUsage_ / stats_.size();
    }

    void TopKeysAnalyser::operator()(const StringView &key, const DataContainer &)
    {
        updateCurrentTime();
        if (auto *stat = getStat(key))
        {
            stat->inc(currentTime_);
            if (stat->totalValue() == 1u)
            {
                lowValueStats_.tryEmplace(stat);
            }
            else
            {
                lowValueStats_.erase(stat);
            }
        }
    }

    std::vector<std::pair<StringView, std::uint64_t>> TopKeysAnalyser::collect()
    {
        updateCurrentTime();
        std::vector<std::pair<StringView, std::uint64_t>> statistic;
        statistic.reserve(stats_.size());
        for (auto statIt = stats_.begin(); statIt != stats_.end();)
        {
            if (statIt->second.outdated(currentTime_))
            {
                statIt = eraseStat(statIt);
                continue;
            }
            statIt->second.dropUnused(currentTime_);
            statistic.emplace_back(statIt->first, statIt->second.totalValue());
            ++statIt;
        }
        return statistic;
    }

    bool TopKeysAnalyser::fitMemory(std::size_t reserve) noexcept
    {
        if (enougthMemory(reserve))
        {
            return true;
        }
        purgeOutdatedStats();
        purgeLowValueStats(reserve);
        return enougthMemory(reserve) || purgeStatsWithSmallValue(reserve);
    }

    void TopKeysAnalyser::purgeOutdatedStats() noexcept
    {
        while (!lruList_.empty())
        {
            const StringView lastKey{lruList_.back()};
            auto statIt = stats_.find(lastKey);
            assert(statIt != stats_.end());
            if (!statIt->second.outdated(currentTime_))
            {
                return;
            }
            eraseStat(statIt);
        }
    }

    void TopKeysAnalyser::purgeLowValueStats(std::size_t reserve) noexcept
    {
        for (auto &statPtr : lowValueStats_.cache)
        {
            if (enougthMemory(reserve))
            {
                return;
            }
            if (statPtr && statPtr->totalValue() == 1)
            {
                eraseStat(stats_.find(*statPtr->lruIt));
            }
            statPtr = nullptr;
        }
    }

    bool TopKeysAnalyser::purgeStatsWithSmallValue(std::size_t reserve) noexcept
    {
        static const auto statsValueCmp =
            [](const StatsStorage::iterator &l, const StatsStorage::iterator &r) {
                return l->second.totalValue() < r->second.totalValue();
            };

        std::set<StatsStorage::iterator, decltype(statsValueCmp)> statsForRemoving{statsValueCmp};
        const std::size_t elementsForRemove{1 + reserve / getStatApproximateMemoryUsage()};
        for (auto it = stats_.begin(); it != stats_.end(); ++it)
        {
            if (statsForRemoving.size() < elementsForRemove)
            {
                statsForRemoving.emplace(it);
                continue;
            }
            auto lastElement = std::prev(statsForRemoving.end());
            if (statsValueCmp(it, *lastElement))
            {
                statsForRemoving.erase(lastElement);
                statsForRemoving.emplace(it);
            }
        }

        for (const auto &statIt : statsForRemoving)
        {
            eraseStat(statIt);
            if (enougthMemory(reserve))
            {
                return true;
            }
        }
        return false;
    }

    bool TopKeysAnalyser::enougthMemory(std::size_t reserve) const noexcept
    {
        return reserve + keysMemoryUsage_ + structsMemoryUsage_ <= memoryLimit_;
    }

    TopKeysAnalyser::StatsStorage::iterator TopKeysAnalyser::eraseStat(StatsStorage::iterator statIt) noexcept
    {
        if (statIt->second.keyNotFromPool)
        {
            keysMemoryUsage_ -= statIt->first.size();
        }
        lruList_.erase(statIt->second.lruIt);
        lowValueStats_.erase(&statIt->second);
        return stats_.erase(statIt);
    }
} // namespace mega_base
