// MegaBase 2018
#ifndef CPPEVENT_BASE
#define CPPEVENT_BASE
#include <memory>

#include <event2/event.h>

namespace cpp_event
{
    class Base
    {
    public:
        Base();

        void dispatch() noexcept;
        int loopBreak() noexcept;

        using EvBase = std::unique_ptr<event_base, void (*)(event_base *)>;
        const EvBase &getBase() const noexcept;

    private:
        EvBase base_;
    };
} //namespace cpp_event

#endif // CPPEVENT_BASE
