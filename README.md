# MegaBase
MegaBase is a super simple single thread in-memory key-value database which supports set and get operations and allows to obtain statistic of top rate keys.

## megabased
`megabased` is daemon of MegaBase itself.

### Parameters
  * `-p, --port` (optional, unsigned integer, default 8081) is a listening port.
  * `-m, --memory` (optional, unsigned integer, default 16 MB) is a memory limit in bytes of top rate keys statistic storage.

### Handlers

#### /set
`/set?key=${key}` allows to set the value for corresponding key.

Should be HTTP POST request, where

  * `${key}` (required, string) is a key and request body contains the value.

If the value successfully accepted, returns 200.

#### /get
`/get?key=${key}` allows to get the value of corresponding key.

Should be HTTP GET request, where

  * `${key}` (required, string) is a requested key.

If the value is present in the database, returns it with 200. If the value is not present, returns 204.

#### /top
`/top?filter=${filter}&order=${order}&limit=${limit}` allows to obtain statistic by top requested (from `/set` and `/get`) keys for last minute.

Should be HTTP GET request, where

  * `${filter}` (optional, string) allows to filter keys. Each key which contains this string will be included into top report;
  * `${order}` (optional, string, default `decreasing`) allows to set order of reported statistic. Any unexpected value of this parameter will be treated as `decreasing`;
     * `increasing` means that on first will be keys with low rate;
     * `decreasing` means that on first will be keys with high rate.
  * `${limit}` (optional, unsigned integer) allows to limit the number of keys in reported statistic. Any unexpected limit value or 0 will be treated as unlimited.

Returns TSV table where the first column is keys and the second column is rates.

### Build
For building `megabased` you need cmake and C++ compilier with C++14 support.
```bash
$ mkdir build
$ cd build
$ cmake ..
$ make
$ ./megabased -v
```
Also, if your default compiler is not clang (e.g. on ubuntu 16 or ubuntu 18), you may build `megabased` with clang toolchain: add `CMAKE_TOOLCHAIN_FILE` parameter in cmake call
```bash
$ cmake -DCMAKE_TOOLCHAIN_FILE=../Clang.cmake ..
```
For debugging you may use [address sanitizer](https://github.com/google/sanitizers/wiki/AddressSanitizer) and [undefined behavior sanitizer](https://clang.llvm.org/docs/UndefinedBehaviorSanitizer.html): add `SAN` parameter in cmake call.

For address sanitizer: add `ASAN`
```bash
$ cmake -DSAN=ASAN ..
```
For undefined behavior sanitizer: add `USAN`
```bash
$ cmake -DSAN=USAN ..
```
The build was tested under ubuntu 16 (gcc 5.4), ubuntu 18 (gcc 7.3) and macOS 10.12 (Apple LLVM version 9.0.0 (clang-900.0.39.2))

## Tools
### Fake Client
In this repository you can also find a [php script](./tools/fake_client.php) for testing stability, performance and correctness of top keys statistic. Fake client sends requests with some write ratio (the number of `/set` requests regarding the number of all requests), prints report, and matches obtained top statistic with real distribution of keys.

#### Parameters
  * `-p` (optional, unsigned integer, default 8081) is a database listening port;
  * `-n` (optional, unsigned integer, default 1000) is a number of outgoing requests;
  * `-c` (optional, unsigned integer, default 16) is a maximum number of simultaneously opened connections to database;
  * `-w` (optional, real, default 0.05) is a write ratio, namely the number of `/set` requests regarding the number of all outgoing requests.

#### Depends
  * [PHP 7](http://php.net/manual/en/install.php);
  * [cURL library](http://php.net/manual/en/book.curl.php).
