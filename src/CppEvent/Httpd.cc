// MegaBase 2018
#include "CppEvent/Httpd.hpp"

#include "CppEvent/Base.hpp"

#include <event2/listener.h>

namespace cpp_event
{
    Httpd::Httpd(Base &base) noexcept
        : httpd_(evhttp_new(base.getBase().get()), evhttp_free)
    {
        evhttp_set_allowed_methods(httpd_.get(), EVHTTP_REQ_GET | EVHTTP_REQ_POST);
        evhttp_set_default_content_type(httpd_.get(), nullptr);
    }

    void Httpd::accept(std::uint16_t port)
    {
        if (auto boundSocket = evhttp_bind_socket_with_handle(httpd_.get(), "0.0.0.0", port))
        {
            evconnlistener_enable(evhttp_bound_socket_get_listener(boundSocket));
            return;
        }
        throw std::runtime_error{"failed to bind socket"};
    }

    const Httpd::EvHttp &Httpd::getEvHttp() const noexcept
    {
        return httpd_;
    }
} // namespace cpp_event
