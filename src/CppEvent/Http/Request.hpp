// MegaBase 2018
#ifndef CPPEVENT_HTTP_REQUEST
#define CPPEVENT_HTTP_REQUEST
#include <event2/http.h>

#include <memory>

namespace cpp_event
{
    namespace http
    {
        enum class Codes : std::uint16_t {
            OK = 200,
            NO_CONTENT = 204,

            BAD_REQUEST = 400,

            SERVER_ERROR = 500
        };

        struct Content
        {
            const char *data;
            std::size_t size;
        };

        class Request
        {
        private:
            explicit Request(evhttp_request *request) noexcept;

        public:
            static std::unique_ptr<Request> build(evhttp_request *request) noexcept
            {
                return std::unique_ptr<Request>{new Request(request)};
            }

            const char *getIncomingParam(const char *key, const char *defaultValue = nullptr) noexcept;
            Content getIncomingContent() noexcept;

            static void sendReply(std::unique_ptr<Request> req, cpp_event::http::Codes code,
                Content content = Content{nullptr, 0u}) noexcept;

            ~Request() noexcept;

        private:
            evhttp_request *request_{nullptr};
            const evhttp_uri *uri_{nullptr};

            std::unique_ptr<evkeyvalq> queryParams_;
        };
    } // namespace http
} // namespace cpp_event

#endif // CPPEVENT_HTTP_REQUEST
