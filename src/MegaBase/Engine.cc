// MegaBase 2018
#include "MegaBase/Engine.hpp"

#include <cstring>

namespace mega_base
{
    void DataContainer::updateData(const char *data, std::size_t size)
    {
        size_ = size;
        void *newDataMem{std::realloc(static_cast<void *>(data_), size_)};
        if (!newDataMem)
        {
            throw std::runtime_error("can't allocate memory for data");
        }
        data_ = static_cast<char *>(newDataMem);
        std::memcpy(data_, data, size);
    }
} // namespace mega_base
