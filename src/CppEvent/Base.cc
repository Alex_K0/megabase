// MegaBase 2018
#include "CppEvent/Base.hpp"

#include <stdexcept>

namespace cpp_event
{
    using EvConfig = std::unique_ptr<event_config, void (*)(event_config *)>;
    EvConfig makeConfig()
    {
        EvConfig evCfg{event_config_new(), event_config_free};
        if (!evCfg)
        {
            throw std::runtime_error{"can't create config for event base"};
        }
        event_config_set_flag(evCfg.get(), EVENT_BASE_FLAG_NOLOCK);
        return evCfg;
    }

    Base::Base()
        : base_(event_base_new_with_config(makeConfig().get()), event_base_free)
    {
        if (!base_)
        {
            throw std::runtime_error{"can't create base with disabled locks"};
        }
    }

    void Base::dispatch() noexcept
    {
        event_base_dispatch(base_.get());
    }

    int Base::loopBreak() noexcept
    {
        return event_base_loopbreak(base_.get());
    }

    const Base::EvBase &Base::getBase() const noexcept
    {
        return base_;
    }
} // namespace cpp_event
