// MegaBase 2018
#ifndef MEGABASE_STRING_VIEW
#define MEGABASE_STRING_VIEW
#include <cstring>
#include <forward_list>
#include <memory>
#include <unordered_set>

#include "MegaBase/MurMurHash.hpp"

namespace mega_base
{
    class StringView
    {
    public:
        StringView(const char *str = nullptr);
        StringView(const char *str, std::size_t size) noexcept;

        std::size_t size() const noexcept { return size_; }
        bool empty() const noexcept { return size() == 0; }
        const char *data() const noexcept { return str_; }
        const char *begin() const noexcept { return str_; }
        const char *end() const noexcept { return str_ + size_; }
        bool storedInPool() const noexcept { return storedInPool_; }

    private:
        const char *str_{nullptr};
        std::size_t size_{0u};

        friend class StringViewPool;
        bool storedInPool_{false};
    };

    inline bool operator==(const StringView &l, const StringView &r) noexcept
    {
        const std::size_t lSize{l.size()};
        const std::size_t rSize{r.size()};
        if (lSize != rSize)
        {
            return false;
        }
        if (l.data() == r.data())
        {
            return true;
        }
        return std::memcmp(l.data(), r.data(), lSize) == 0;
    }

    inline bool operator!=(const StringView &l, const StringView &r) noexcept
    {
        return !(l == r);
    }

} // namespace mega_base

namespace std
{
    template <>
    struct hash<mega_base::StringView>
    {
        size_t operator()(const mega_base::StringView &s) const noexcept
        {
            return mega_base::calculateMurMurHash(s.data(), s.size());
        }
    };
} // namespace std

namespace mega_base
{
    class StringViewPool
    {
    public:
        explicit StringViewPool(std::size_t initChunkCapacity = 4u * 1024u * 1024u) noexcept;

        StringView insert(const StringView &str) noexcept;

    private:
        std::unordered_set<StringView> cache_;

        struct Chunk
        {
            explicit Chunk(std::size_t initCapacity) noexcept;

            std::unique_ptr<char[]> data;
            std::size_t capacity{0u};
            std::size_t size{0u};
        };
        std::forward_list<Chunk> chunks_;
    };
} // namespace mega_base
#endif // MEGABASE_STRING_VIEW
