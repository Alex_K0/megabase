// MegaBase 2018
#include "CppEvent/Http/Request.hpp"

#include <event2/buffer.h>
#include <event2/keyvalq_struct.h>

#include <memory>

namespace cpp_event
{
    namespace http
    {
        Request::Request(evhttp_request *request) noexcept
            : request_(request),
              uri_(evhttp_request_get_evhttp_uri(request_))
        {
        }

        const char *Request::getIncomingParam(const char *key, const char *defaultValue) noexcept
        {
            if (!queryParams_)
            {
                queryParams_ = std::make_unique<evkeyvalq>();
                evhttp_parse_query_str(evhttp_uri_get_query(uri_), queryParams_.get());
            }
            if (const char *value = evhttp_find_header(queryParams_.get(), key))
            {
                return value;
            }
            return defaultValue;
        }

        Content Request::getIncomingContent() noexcept
        {
            auto incomingBuffer = evhttp_request_get_input_buffer(request_);
            const char *content{reinterpret_cast<const char *>(evbuffer_pullup(incomingBuffer, -1))};
            return {content, evbuffer_get_length(incomingBuffer)};
        }

        void Request::sendReply(std::unique_ptr<Request> req,
            cpp_event::http::Codes code, Content content) noexcept
        {
            auto outgoingBuffer = evhttp_request_get_output_buffer(req->request_);
            evbuffer_add(outgoingBuffer, content.data, content.size);
            evhttp_send_reply(req->request_, static_cast<int>(code), nullptr, nullptr);
            req.reset();
        }

        Request::~Request() noexcept
        {
            if (queryParams_)
            {
                evhttp_clear_headers(queryParams_.get());
            }
        }
    } // namespace http
} // namespace cpp_event
