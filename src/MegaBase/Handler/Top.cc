// MegaBase 2018
#include "MegaBase/Handler/Top.hpp"

#include "CppEvent/Http/Request.hpp"
#include "MegaBase/TopKeysAnalyser.hpp"

#include <algorithm>
#include <iostream>

namespace mega_base
{
    namespace
    {
        template <typename Cmp>
        void sortStats(std::vector<std::pair<StringView, std::uint64_t>> &stats, const Cmp &cmp)
        {
            std::sort(stats.begin(), stats.end(),
                [&cmp](const auto &l, const auto &r) {
                    return cmp(l.second, r.second);
                });
        }
    } // anonymous namespace

    namespace handler
    {
        Top::Top(TopKeysAnalyser &top) noexcept
            : top_(top)
        {
        }

        void Top::operator()(std::unique_ptr<cpp_event::http::Request> req) noexcept
        {
            try
            {
                auto stats = top_.collect();
                std::uint64_t total{0u};
                for (const auto &stat : stats)
                {
                    total += stat.second;
                }

                if (const char *filter = req->getIncomingParam("filter"))
                {
                    const char *filterEnd{filter + std::strlen(filter)};
                    if (filter != filterEnd)
                    {
                        auto statEndIt = std::remove_if(stats.begin(), stats.end(),
                            [filter, filterEnd](const auto &stat) {
                                auto charIt = std::search(stat.first.begin(), stat.first.end(), filter, filterEnd);
                                return charIt == stat.first.end();
                            });
                        stats.erase(statEndIt, stats.end());
                    }
                }

                const char *order = req->getIncomingParam("order", "decreasing");
                if (std::strcmp(order, "increasing") == 0)
                {
                    sortStats(stats, std::less<std::uint64_t>());
                }
                else
                {
                    sortStats(stats, std::greater<std::uint64_t>());
                }

                if (const char *limit = req->getIncomingParam("limit"))
                {
                    const std::size_t topSize{std::strtoul(limit, nullptr, 10)};
                    if (topSize && topSize < stats.size())
                    {
                        stats.resize(topSize);
                    }
                }

                std::string report;
                for (const auto &stat : stats)
                {
                    const double rate{static_cast<double>(stat.second) / static_cast<double>(total)};
                    report.append(stat.first.begin(), stat.first.end());
                    report.append(1, '\t').append(std::to_string(rate)).append(1, '\n');
                }
                cpp_event::http::Request::sendReply(std::move(req),
                    cpp_event::http::Codes::OK, {report.data(), report.size()});
            }
            catch (const std::exception &ex)
            {
                std::cerr << "Got an exception on /top request; " << ex.what() << std::endl;
                if (req)
                {
                    cpp_event::http::Request::sendReply(std::move(req), cpp_event::http::Codes::SERVER_ERROR);
                }
            }
        }
    } //namespace handler
} // namespace mega_base