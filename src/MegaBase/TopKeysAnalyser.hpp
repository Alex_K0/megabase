// MegaBase 2018
#ifndef MEGABASE_TOPKEYSANALYSER
#define MEGABASE_TOPKEYSANALYSER
#include "MegaBase/StringView.hpp"
#include "MegaBase/TrackedAllocator.hpp"

#include <array>
#include <chrono>
#include <cmath>
#include <list>
#include <memory>
#include <unordered_map>
#include <vector>

namespace mega_base
{
    class DataContainer;

    class TopKeysAnalyser
    {
    public:
        TopKeysAnalyser(const TopKeysAnalyser &) = delete;
        TopKeysAnalyser &operator=(const TopKeysAnalyser &) = delete;

        TopKeysAnalyser(std::size_t memoryLimit);

        void operator()(const StringView &key, const DataContainer &dataContainer);
        std::vector<std::pair<StringView, std::uint64_t>> collect();

    private:
        void updateCurrentTime() noexcept;

        bool fitMemory(std::size_t reserve) noexcept;
        void purgeOutdatedStats() noexcept;
        void purgeLowValueStats(std::size_t reserve) noexcept;
        bool purgeStatsWithSmallValue(std::size_t reserve) noexcept;
        bool enougthMemory(std::size_t reserve) const noexcept;

        using LruList = std::list<StringView, TrackedAllocator<StringView>>;

        class Stat
        {
        public:
            Stat() noexcept;

            void inc(std::chrono::seconds currentTime) noexcept;
            void dropUnused(std::chrono::seconds currentTime) noexcept;
            bool outdated(std::chrono::seconds currentTime) const noexcept;
            std::size_t totalValue() const noexcept { return totalValue_; }

            std::unique_ptr<char[]> keyNotFromPool;
            LruList::iterator lruIt;

        private:
            static constexpr std::uint32_t KEEP_SECONDS{60u};

            std::chrono::seconds lastAccess_{0};
            std::uint64_t totalValue_{0u};
            std::array<std::uint32_t, KEEP_SECONDS> histogram_;
        };

        Stat *getStat(const StringView &key);
        Stat &insertStat(const StringView &key);
        void updateStat(Stat &stat) noexcept;
        std::size_t getStatApproximateMemoryUsage() const noexcept;

        using StatsStorage = std::unordered_map<StringView, Stat, std::hash<StringView>,
            std::equal_to<StringView>, TrackedAllocator<std::pair<const StringView, Stat>>>;

        StatsStorage::iterator eraseStat(StatsStorage::iterator statIt) noexcept;

        class LowValueStats
        {
        public:
            LowValueStats() noexcept;

            void erase(Stat *stat) noexcept;
            void tryEmplace(Stat *stat) noexcept;

            std::array<Stat *, 37u> cache;
        };

        std::chrono::seconds currentTime_{0};
        const std::size_t memoryLimit_{0u};
        std::size_t keysMemoryUsage_{0u};
        std::size_t structsMemoryUsage_{0u};
        LowValueStats lowValueStats_;
        LruList lruList_;
        StatsStorage stats_;
    };
} // namespace mega_base

#endif //MEGABASE_TOPKEYSANALYSER
