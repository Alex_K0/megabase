// MegaBase 2018
#include <cstdint>

namespace mega_base
{
    inline std::uint64_t calculateMurMurHash(const void *key, std::size_t len, std::uint32_t seed = 14217935)
    {
        const std::uint64_t m{0xc6a4a7935bd1e995};
        const std::int32_t r{47};

        std::uint64_t h{seed ^ (len * m)};

        const std::uint64_t *data{reinterpret_cast<const std::uint64_t *>(key)};
        const std::uint64_t *end{data + (len / 8)};

        while (data != end)
        {
            std::uint64_t k{*data++};

            k *= m;
            k ^= k >> r;
            k *= m;

            h ^= k;
            h *= m;
        }

        const std::uint8_t *data2{reinterpret_cast<const unsigned char *>(data)};
        switch (len & 7)
        {
            case 7:
                h ^= (std::uint64_t(data2[6]) << 48);
            case 6:
                h ^= (std::uint64_t(data2[5]) << 40);
            case 5:
                h ^= (std::uint64_t(data2[4]) << 32);
            case 4:
                h ^= (std::uint64_t(data2[3]) << 24);
            case 3:
                h ^= (std::uint64_t(data2[2]) << 16);
            case 2:
                h ^= (std::uint64_t(data2[1]) << 8);
            case 1:
                h ^= std::uint64_t(data2[0]);
                h *= m;
            default:
                break;
        };

        h ^= h >> r;
        h *= m;
        h ^= h >> r;
        return h;
    }
} // namespace mega_base
