// MegaBase 2018
#ifndef MEGABASE_RUNSERVER
#define MEGABASE_RUNSERVER
#include <cstddef>
#include <cstdint>

namespace mega_base
{
    struct ServerConfig
    {
        std::uint16_t port{8081u};
        std::size_t topMemoryLimit{1024u * 1024u * 16u};
    };

    int runServer(const ServerConfig &config);
} // namespace mega_base

#endif //MEGABASE_RUNSERVER
