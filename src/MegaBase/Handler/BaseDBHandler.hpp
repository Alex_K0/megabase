// MegaBase 2018
#ifndef MEGABASE_BASEHANDLER
#define MEGABASE_BASEHANDLER
#include "MegaBase/Engine.hpp"

namespace mega_base
{
    namespace handler
    {
        template <typename... Observers>
        class BaseDBHandler
        {
        public:
            BaseDBHandler(Engine<Observers...> &engine) noexcept : engine_(engine) {}

        protected:
            Engine<Observers...> &engine_;
        };
    } // namespace handler

    template <template <typename...> class HadlerType, typename... Observers>
    HadlerType<Observers...> makeDBHandler(Engine<Observers...> &engine) noexcept
    {
        return HadlerType<Observers...>{engine};
    }

} // namespace mega_base
#endif //MEGABASE_BASEHANDLER
