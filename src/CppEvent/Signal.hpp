// MegaBase 2018
#ifndef CPPEVENT_SIGNAL
#define CPPEVENT_SIGNAL
#include "CppEvent/Base.hpp"

#include <event2/event.h>
#include <event2/event_struct.h>

#include <memory>

namespace cpp_event
{
    template <typename SignalHandler>
    class Signal
    {
    public:
        Signal(Signal &&) = default;

        Signal(Base &base, int signum, SignalHandler &handler)
            : event_(std::make_unique<event>())
        {
            if (event_assign(event_.get(), base.getBase().get(), signum, EV_SIGNAL | EV_PERSIST,
                    [](int signal, short, void *arg) {
                        (*static_cast<SignalHandler *>(arg))(signal);
                    },
                    &handler))
            {
                throw std::runtime_error{"can't create signal handler event"};
            }
            if (event_add(event_.get(), nullptr))
            {
                throw std::runtime_error{"can't enable signal handler event"};
            }
        }

        ~Signal() noexcept
        {
            if (event_)
            {
                event_del(event_.get());
            }
        }

    private:
        std::unique_ptr<event> event_;
    };

    template <typename SignalHandler>
    Signal<SignalHandler> makeSignal(Base &base, int signum, SignalHandler &handler)
    {
        return {base, signum, handler};
    }
} // namespace cpp_event

#endif //CPPEVENT_SIGNAL
