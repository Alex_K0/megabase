// MegaBase 2018
#ifndef MEGABASE_TRACKED_ALLOC
#define MEGABASE_TRACKED_ALLOC
#include <memory>

namespace mega_base
{
    template <typename T>
    class TrackedAllocator : private std::allocator<T>
    {
        template <typename>
        friend class TrackedAllocator;

    public:
        using typename std::allocator<T>::value_type;
        using typename std::allocator<T>::pointer;
        using typename std::allocator<T>::const_pointer;
        using typename std::allocator<T>::reference;
        using typename std::allocator<T>::const_reference;

        using std::allocator<T>::construct;
        using std::allocator<T>::destroy;

        template <class U>
        struct rebind
        {
            using other = TrackedAllocator<U>;
        };

        TrackedAllocator(std::size_t &currentMemory) noexcept
            : currentMemory_(currentMemory)
        {
        }

        template <class U>
        TrackedAllocator(const TrackedAllocator<U> &alloc) noexcept
            : std::allocator<T>(alloc),
              currentMemory_(alloc.currentMemory_)
        {
        }

        pointer allocate(std::size_t n, const_pointer hint = 0) noexcept
        {
            auto mem = std::allocator<T>::allocate(n, hint);
            currentMemory_ += toSize(n);
            return mem;
        }

        void deallocate(pointer p, std::size_t n) noexcept
        {
            std::allocator<T>::deallocate(p, n);
            currentMemory_ -= toSize(n);
        }

        friend bool operator==(const TrackedAllocator &l, const TrackedAllocator &r) noexcept
        {
            return &l.currentMemory_ == &r.currentMemory_;
        }

        friend bool operator!=(const TrackedAllocator &l, const TrackedAllocator &r) noexcept
        {
            return !(l == r);
        }

    private:
        static std::size_t toSize(std::size_t n) noexcept { return sizeof(T) * n; }

        std::size_t &currentMemory_;
    };
} // namespace mega_base

#endif // MEGABASE_TRACKED_ALLOC
