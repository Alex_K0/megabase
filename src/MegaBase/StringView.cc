// MegaBase 2018
#include "MegaBase/StringView.hpp"

#include <cassert>
#include <limits>

namespace mega_base
{
    StringView::StringView(const char *str)
        : str_(str),
          size_(str ? std::strlen(str) : 0)
    {
    }

    StringView::StringView(const char *str, std::size_t size) noexcept
        : str_(str),
          size_(size)
    {
    }

    StringViewPool::Chunk::Chunk(std::size_t initCapacity) noexcept
        : data(std::make_unique<char[]>(initCapacity)),
          capacity(initCapacity)
    {
    }

    StringViewPool::StringViewPool(std::size_t initChunkCapacity) noexcept
    {
        chunks_.emplace_front(initChunkCapacity);
    }

    StringView StringViewPool::insert(const StringView &str) noexcept
    {
        auto it = cache_.find(str);
        if (it != cache_.end())
        {
            return *it;
        }
        Chunk *chunkPtr{&chunks_.front()};
        const std::size_t strSize{str.size()};
        if (chunkPtr->size + strSize > chunkPtr->capacity)
        {
            const std::size_t nextChunkCapacity{std::max(strSize, chunkPtr->capacity * 2)};
            chunks_.emplace_front(nextChunkCapacity);
            chunkPtr = &chunks_.front();
        }

        std::memcpy(&chunkPtr->data[chunkPtr->size], str.data(), strSize);
        StringView cachedStr{&chunkPtr->data[chunkPtr->size], strSize};
        cachedStr.storedInPool_ = true;
        chunkPtr->size += strSize;
        return *cache_.emplace(cachedStr).first;
    }
} // namespace mega_base