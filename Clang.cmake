# MegaBase 2018
find_program(CLANG_C_COMPILER NAMES clang)
if(NOT CLANG_C_COMPILER)
    message(FATAL_ERROR "Can't found clang c compilier")
endif()

find_program(CLANG_CXX_COMPILER NAMES clang++)
if(NOT CLANG_CXX_COMPILER)
    message(FATAL_ERROR "Can't found clang c++ compilier")
endif()

set (CMAKE_C_COMPILER ${CLANG_C_COMPILER})
set (CMAKE_CXX_COMPILER ${CLANG_CXX_COMPILER})

