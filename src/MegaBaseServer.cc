// MegaBase 2018
#include "MegaBase/RunServer.hpp"

#include <cassert>
#include <exception>
#include <getopt.h>
#include <iostream>
#include <limits>
#include <stdexcept>
#include <vector>

struct Options
{
    mega_base::ServerConfig cfg;
    bool showHelp{false};
    bool showVersion{false};
};

struct BadArguments : std::runtime_error
{
    explicit BadArguments(const std::string &msg)
        : std::runtime_error(msg)
    {
    }
};

struct Opt
{
    option opt;
    std::string description;
};

static const std::vector<Opt> ALLOWED_OPTS{
    {{"help", no_argument, nullptr, 'h'}, "show help"},
    {{"version", no_argument, nullptr, 'v'}, "show version"},
    {{"port", optional_argument, nullptr, 'p'}, "listening port"},
    {{"memory", optional_argument, nullptr, 'm'}, "top memory limit"}};

Options parse(int argc, char *argv[])
{
    opterr = 0;

    static option LONG_OPTS[] = {
        ALLOWED_OPTS.at(0).opt,
        ALLOWED_OPTS.at(1).opt,
        ALLOWED_OPTS.at(2).opt,
        ALLOWED_OPTS.at(3).opt,
        {nullptr, 0, nullptr, 0}};

    Options opts;
    while (1)
    {
        const int opt{getopt_long(argc, argv, "hvp:m:", LONG_OPTS, nullptr)};
        if (opt == -1)
        {
            break;
        }

        switch (opt)
        {
            case 'h':
                opts.showHelp = true;
                break;
            case 'v':
                opts.showVersion = true;
                break;
            case 'p':
            {
                const std::uint64_t port{std::strtoul(optarg, nullptr, 10)};
                if (port == 0 || port > std::numeric_limits<std::uint16_t>::max())
                {
                    throw BadArguments("the port value should be an integer > 0 and <= " +
                                       std::to_string(std::numeric_limits<std::uint16_t>::max()));
                }
                opts.cfg.port = static_cast<std::uint16_t>(port);
                break;
            }
            case 'm':
                opts.cfg.topMemoryLimit = std::strtoul(optarg, nullptr, 10);
                if (opts.cfg.topMemoryLimit == 0)
                {
                    throw BadArguments("the memory limit should be an integer > 0 and <= " +
                                       std::to_string(std::numeric_limits<std::uint64_t>::max()));
                }
                break;
            default:
                std::string unrecognized;
                if (optopt)
                {
                    unrecognized.append(1, '-').append(1, static_cast<char>(optopt));
                }
                else
                {
                    unrecognized.append(argv[optind - 1]);
                }
                throw BadArguments("bad option '" + unrecognized + "'");
        }
    }

    if (optind < argc)
    {
        std::string nonOpts{"'" + std::string{argv[optind++]} + "'"};
        while (optind < argc)
        {
            nonOpts.append(", '").append(argv[optind++]).append(1, '\'');
        }
        throw BadArguments("non-option elements: " + nonOpts);
    }
    return opts;
}

void showHelp() noexcept
{
    std::cout << "Allowed options:" << std::endl;
    for (const auto &allowedOpt : ALLOWED_OPTS)
    {
        std::cout << "\t-" << static_cast<char>(allowedOpt.opt.val) << " [ --" << allowedOpt.opt.name << " ]"
                  << (allowedOpt.opt.has_arg ? " args " : " ") << allowedOpt.description << std::endl;
    }
}

void showVersion() noexcept
{
    constexpr char version[]{"0.0.1"};
    std::cout << "megabase " << version << std::endl;
}

int main(int argc, char *argv[])
{
    try
    {
        auto options = parse(argc, argv);
        if (options.showHelp)
        {
            showHelp();
            showVersion();
            return 0;
        }

        if (options.showVersion)
        {
            showVersion();
            return 0;
        }
        return mega_base::runServer(options.cfg);
    }
    catch (const BadArguments &ex)
    {
        std::cerr << "Bad program arguments; " << ex.what() << std::endl;
        std::cerr << "Try to use --help" << std::endl;
        return 1;
    }
    catch (const std::exception &ex)
    {
        std::cerr << "Can't start server; " << ex.what() << std::endl;
        return 1;
    }
}
