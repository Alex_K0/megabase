// MegaBase 2018
#ifndef MEGABASE_ENGINE
#define MEGABASE_ENGINE
#include "MegaBase/StringView.hpp"

#include <cstdlib>
#include <unordered_map>

namespace mega_base
{
    class DataContainer
    {
    public:
        DataContainer() = default;
        DataContainer(const DataContainer &) = delete;
        DataContainer &operator=(const DataContainer &) = delete;

        const char *data() const noexcept { return data_; }
        std::size_t size() const noexcept { return size_; }

        bool hasData() const noexcept { return data_; }
        void updateData(const char *data, std::size_t size);

        ~DataContainer() noexcept { std::free(data_); }

    private:
        char *data_{nullptr};
        std::size_t size_{0u};
    };

    template <typename... Observers>
    class Engine
    {
    public:
        explicit Engine(Observers &... observers)
            : observers_(observers...)
        {
        }

        const DataContainer &get(StringView key)
        {
            static const DataContainer noData;
            auto it = db_.find(key);
            if (it == db_.end())
            {
                notifyObservers(key, noData);
                return noData;
            }
            notifyObservers(it->first, it->second);
            return it->second;
        }

        void set(StringView key, const char *data, std::size_t size)
        {
            StringView cachedKey{keyPool_.insert(key)};
            DataContainer &dataContainer{db_[cachedKey]};
            dataContainer.updateData(data, size);
            notifyObservers(cachedKey, dataContainer);
        }

    private:
        using ObserversTuple = std::tuple<Observers &...>;

        template <std::size_t I = 0>
        std::enable_if_t<I == std::tuple_size<ObserversTuple>::value, void>
        notifyObservers(const StringView &, const DataContainer &)
        {
        }

        // clang-format off
        template <std::size_t I = 0>
        std::enable_if_t<I < std::tuple_size<ObserversTuple>::value, void>
        notifyObservers(const StringView &key, const DataContainer &data)
        {
            std::get<I>(observers_)(key, data);
            notifyObservers<I + 1>(key, data);
        }
        // clang-format on

        StringViewPool keyPool_;
        std::unordered_map<StringView, DataContainer> db_;
        ObserversTuple observers_;
    };
} // namespace mega_base

#endif //MEGABASE_ENGINE
