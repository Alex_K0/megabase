// MegaBase 2018
#include "MegaBase/RunServer.hpp"

#include "CppEvent/Base.hpp"
#include "CppEvent/Httpd.hpp"
#include "CppEvent/Signal.hpp"
#include "MegaBase/Engine.hpp"
#include "MegaBase/Handler/Get.hpp"
#include "MegaBase/Handler/Set.hpp"
#include "MegaBase/Handler/Top.hpp"
#include "MegaBase/TopKeysAnalyser.hpp"

#include <iostream>
#include <signal.h>
#include <string.h>

namespace mega_base
{
    int runServer(const ServerConfig &config)
    {
        std::cout << "Starting megabase server" << std::endl;
        cpp_event::Base eventBase;

        auto stopHander = [&eventBase](int signum) {
            std::cout << "Got '" << strsignal(signum) << "'; megabase server is turning off" << std::endl;
            eventBase.loopBreak();
        };

        auto sigInt = cpp_event::makeSignal(eventBase, SIGINT, stopHander);
        auto sigTerm = cpp_event::makeSignal(eventBase, SIGTERM, stopHander);
        auto sigQuit = cpp_event::makeSignal(eventBase, SIGQUIT, stopHander);

        auto nullHandler = [](int) {};
        auto sigPipe = cpp_event::makeSignal(eventBase, SIGPIPE, nullHandler);

        TopKeysAnalyser keysAnalyser{config.topMemoryLimit};
        Engine<TopKeysAnalyser> engine{keysAnalyser};
        auto getHandler = makeDBHandler<handler::Get>(engine);
        auto setHandler = makeDBHandler<handler::Set>(engine);
        handler::Top topHandler{keysAnalyser};

        cpp_event::Httpd httpd{eventBase};
        httpd.attach(getHandler, "/get");
        httpd.attach(setHandler, "/set");
        httpd.attach(topHandler, "/top");

        std::cout << "Starting to listen " << config.port << " port" << std::endl;
        httpd.accept(config.port);

        eventBase.dispatch();
        return 0;
    }
} // namespace mega_base
