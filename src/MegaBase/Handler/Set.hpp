// MegaBase 2018
#ifndef MEGABASE_HANDLER_SET
#define MEGABASE_HANDLER_SET

#include "CppEvent/Http/Request.hpp"
#include "MegaBase/Handler/BaseDBHandler.hpp"

#include <iostream>
#include <memory>

namespace mega_base
{
    namespace handler
    {
        template <typename... Observers>
        class Set : private BaseDBHandler<Observers...>
        {
        private:
            using BaseClass = BaseDBHandler<Observers...>;

        public:
            using BaseClass::BaseDBHandler;

            void operator()(std::unique_ptr<cpp_event::http::Request> req) noexcept
            {
                try
                {
                    if (const char *key{req->getIncomingParam("key")})
                    {
                        auto value = req->getIncomingContent();
                        BaseClass::engine_.set(key, value.data, value.size);
                        cpp_event::http::Request::sendReply(std::move(req), cpp_event::http::Codes::OK);
                        return;
                    }
                    cpp_event::http::Request::sendReply(std::move(req), cpp_event::http::Codes::BAD_REQUEST);
                }
                catch (const std::exception &ex)
                {
                    std::cerr << "Got an exception on /set request; " << ex.what() << std::endl;
                    if (req)
                    {
                        cpp_event::http::Request::sendReply(std::move(req), cpp_event::http::Codes::SERVER_ERROR);
                    }
                }
            }
        };
    } // namespace handler
} // namespace mega_base

#endif //MEGABASE_HANDLER_SET
