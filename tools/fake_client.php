#!/usr/bin/php
<?php

$GLOBALS["db"] = "http://localhost";
$GLOBALS["distribution"] = [
    [0, 9],
    [10, 99],
    [100, 999],
    [1000, 9999],
    [10000, 99999],
    [100000, 999999]
];

function generate_key_id()
{
    $bucket = mt_rand(0, count($GLOBALS["distribution"]) - 1);
    return mt_rand(
        $GLOBALS["distribution"][$bucket][0],
        $GLOBALS["distribution"][$bucket][1]);
}

function buckets_count()
{
    return count($GLOBALS["distribution"]);
}

function get_bucket_id_by_key_id($key_id)
{
    $buckets_count = buckets_count();
    for ($bucket_id = 0; $bucket_id < $buckets_count; ++$bucket_id)
    {
        $bucket = $GLOBALS["distribution"][$bucket_id];
        if ($bucket[0] <= $key_id && $key_id <= $bucket[1])
        {
            return $bucket_id;
        }
    }
    return null;
}

function keys_in_bucket($bucket_id)
{
    $bucket = $GLOBALS["distribution"][$bucket_id];
    return 1 + $bucket[1] - $bucket[0];
}

function get_rate_by_bucket_id($bucket_id)
{
    $bucket_rate = 1.0 / buckets_count();
    return $bucket_rate / keys_in_bucket($bucket_id);
}

function make_key($key_id)
{
    return str_repeat($key_id."this_is_not_so_long_test_key_id", 32);
}

function extract_key_id($key)
{
    return intval($key);
}

function make_value($key_id)
{
    return str_repeat($key_id."this_is_not_so_long_body_for_test_key_id", 128);
}

function check_top_stats($db_port)
{
    print("\nCheking top stats..\n");
    $channel = curl_init();
    curl_setopt($channel, CURLOPT_HEADER, false);
    curl_setopt($channel, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($channel, CURLOPT_URL, $GLOBALS["db"].":".$db_port."/top");
    $top_stats_str = curl_exec($channel);
    $resp_code = curl_getinfo($channel, CURLINFO_RESPONSE_CODE);
    curl_close($channel);
    if ($resp_code != 200)
    {
        print("Can't get top stats\n");
        return;
    }

    $stat_in_buckets = [];
    $buckets_count = buckets_count();
    for ($bucket_id = 0; $bucket_id < $buckets_count; ++$bucket_id)
    {
        $stat_in_buckets[] = [];
    }

    $max_delta = 0;
    $max_delta_key_id = 0;
    $max_delta_got = 0;
    $max_delta_expected = 0;

    $top_lines = explode("\n", $top_stats_str);
    foreach ($top_lines as $line)
    {
        if (empty($line))
        {
            continue;
        }

        $key_rate_pair = explode("\t", $line);
        if (count($key_rate_pair) != 2)
        {
            print("Bad top stats format\n");
            return;
        }

        $key_id = extract_key_id($key_rate_pair[0]);
        $bucket_id = get_bucket_id_by_key_id($key_id);
        $stat_in_buckets[$bucket_id][$key_id] = true;

        $got_rate = doubleval($key_rate_pair[1]);
        $expected_rate = get_rate_by_bucket_id($bucket_id);
        $delta = abs($expected_rate - $got_rate);
        if ($delta > $max_delta)
        {
            $max_delta = $delta;
            $max_delta_key_id = $key_id;
            $max_delta_got = $got_rate;
            $max_delta_expected = $expected_rate;
        }
    }

    for ($bucket_id = 0; $bucket_id < $buckets_count; ++$bucket_id)
    {
        if (keys_in_bucket($bucket_id) > count($stat_in_buckets[$bucket_id]))
        {
            $expected_rate = get_rate_by_bucket_id($bucket_id);
            if ($expected_rate > $max_delta)
            {
                $max_delta = $expected_rate;
                $max_delta_key_id = "missed key from bucket ".$bucket_id;
                $max_delta_got = 0;
                $max_delta_expected = $expected_rate;
            }
        }
    }

    print("Max deviation\n");
    print("   key id:\t".$max_delta_key_id."\n");
    printf("   top stat:\t%.9f\n", $max_delta_got);
    printf("   expected:\t%.9f\n", $max_delta_expected);
    printf("   deviation:\t%.9f\n", $max_delta);
}

function setup_request(& $channel_info, $db_port, $write_rate, $multi_channels)
{
    $is_write = lcg_value() <= $write_rate;
    $channel_info["is_write"] = $is_write;
    $key_id = generate_key_id();
    $channel_info["key_id"] = $key_id;
    $channel = $channel_info["channel"];
    curl_multi_add_handle($multi_channels, $channel);
    if ($is_write)
    {
        curl_setopt($channel, CURLOPT_URL, $GLOBALS["db"].":".$db_port."/set?key=".make_key($key_id));
        curl_setopt($channel, CURLOPT_POST, true);
        curl_setopt($channel, CURLOPT_POSTFIELDS, make_value($key_id));
    }
    else
    {
        curl_setopt($channel, CURLOPT_URL, $GLOBALS["db"].":".$db_port."/get?key=".make_key($key_id));
        curl_setopt($channel, CURLOPT_HTTPGET, true);
    }
}

function make_channel_pool($multi_channels, $size)
{
    $pool = [];
    for ($i = 0; $i < $size; ++$i)
    {
        $channel = curl_init();
        curl_setopt($channel, CURLOPT_HEADER, false);
        curl_setopt($channel, CURLOPT_RETURNTRANSFER, true);
        $pool[] = [
            "channel" => $channel,
            "key_id" => null,
            "is_write" => null
        ];
    }
    return $pool;
}

function print_progress($current, $total, & $prev)
{
    $current_progres = 100 * $current / $total;
    if ($current_progres >= $prev + 10)
    {
        print ("Completed ".$current." requests\n");
        $prev = $current_progres;
    }
}

function check_reposne(& $channel_info, & $time_statistic,
    & $get_no_content, & $get_expected, & $get_unexpected,
    & $set_value, & $fails)
{
    $channel = $channel_info["channel"];
    $code = curl_getinfo($channel, CURLINFO_RESPONSE_CODE);
    if ($code == 204)
    {
        ++$get_no_content;
        $time_statistic[] = curl_getinfo($channel, CURLINFO_TOTAL_TIME);
        return;
    }
    if ($code == 200)
    {
        $time_statistic[] = curl_getinfo($channel, CURLINFO_TOTAL_TIME);
        if ($channel_info["is_write"])
        {
            ++$set_value;
            return;
        }
        $value = curl_multi_getcontent($channel);
        $expected_value = make_value($channel_info["key_id"]);
        $value == $expected_value ? ++$get_expected : ++$get_unexpected;
        return;
    }
    ++$fails;
}

function main()
{
    $args = getopt("p:n:c:w:");
    $db_port = $args["p"] ?? 8081;
    $requests_count = $args["n"] ?? 1000;
    $max_connections = $args["c"] ?? 16;
    $write_rate = $args["w"] ?? 0.05;

    $start = gettimeofday(true);

    print("Starting to send ".$requests_count." requests..\n\n");

    $completed = 0;
    $fails = 0;
    $get_no_content = 0;
    $get_unexpected = 0;
    $get_expected = 0;
    $set_value = 0;
    $time_statistic = [];

    $multi_channels = curl_multi_init();
    curl_multi_setopt($multi_channels, CURLMOPT_MAX_HOST_CONNECTIONS, $max_connections);
    curl_multi_setopt($multi_channels, CURLMOPT_PIPELINING, 1);
    $channel_pool = make_channel_pool($multi_channels, $max_connections);
    $on_flight = 0;
    $progress = 0;
    $active = null;
    do
    {
        curl_multi_exec($multi_channels, $active);
        if ($active > 0)
        {
            curl_multi_select($multi_channels);
            continue;
        }

        for ($i = 0; $i < $on_flight; ++$i)
        {
            check_reposne($channel_pool[$i], $time_statistic,
                $get_no_content, $get_expected, $get_unexpected,
                $set_value, $fails);
            curl_multi_remove_handle($multi_channels, $channel_pool[$i]["channel"]);
        }

        print_progress($completed, $requests_count, $progress);
        $left_requests = $requests_count - $completed;
        $on_flight = min($max_connections, $left_requests);
        $completed += $on_flight;
        for ($i = 0; $i < $on_flight; ++$i)
        {
            setup_request($channel_pool[$i], $db_port, $write_rate, $multi_channels);
        }
    } while ($left_requests > 0 || $active > 0);

    foreach ($channel_pool as $channel_info)
    {
        curl_close($channel_info["channel"]);
    }

    print("\nAll requests have sent.\n");

    curl_multi_close($multi_channels);
    sort($time_statistic);

    $end = gettimeofday(true);

    print("\nReport\n");
    print("Max connections:\t".$max_connections."\n");
    printf("Time taken for tests:\t%.6f sec\n", $end - $start);
    print("Completed requests:\t".$completed." requests\n");
    print("Failed requests:\t".$fails." requests\n");

    if (!empty($time_statistic))
    {
        $elements = count($time_statistic);
        $total_requests_time = array_sum($time_statistic);
        printf("Requests per sec:\t%.2f req/sec\n", $elements / $total_requests_time);
        print("\nPercentage of the requests served\n");
        printf("   Avg:\t%.6f sec\n", $total_requests_time / $elements);
        printf("   Min:\t%.6f sec\n", $time_statistic[0]);
        printf("   25%%:\t%.6f sec\n", $time_statistic[$elements * 25 / 100]);
        printf("   50%%:\t%.6f sec\n", $time_statistic[$elements * 50 / 100]);
        printf("   75%%:\t%.6f sec\n", $time_statistic[$elements * 75 / 100]);
        printf("   95%%:\t%.6f sec\n", $time_statistic[$elements * 95 / 100]);
        printf("   99%%:\t%.6f sec\n", $time_statistic[$elements * 99 / 100]);
        printf("   Max:\t%.6f sec\n", $time_statistic[$elements - 1]);
    }

    print("\nRequests\n");
    print("set value:\t".$set_value." requests\n");
    print("get no content:\t".$get_no_content." requests\n");
    print("get expected:\t".$get_expected." requests\n");
    print("get unexpected:\t".$get_unexpected." requests\n");

    check_top_stats($db_port);
}

main();

?>
